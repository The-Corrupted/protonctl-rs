pub const MAX_PER_PAGE: u8 = 50;

pub const GE_PROJECT_OWNER: &str = "GloriousEggroll";

pub const UMU_PROJECT_OWNER: &str = "Open-Wine-Components";

pub const WINE_PROJECT_NAME: &str = "wine-ge-custom";

pub const PROTON_PROJECT_NAME: &str = "proton-ge-custom";

pub const UMU_PROJECT_NAME: &str = "umu-proton";

pub const DOWNLOAD_PATH: &str = ".local/share/protonctl";
